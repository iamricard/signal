import test from 'ava'
import { toWords, outOfBoundsException } from './index'

test('eleven', (t) => {
  t.is('eleven', toWords(11))
})

test('ten', (t) => {
  t.is('ten', toWords(10))
})

test('twenty one', (t) => {
  t.is('twenty-one', toWords(21))
})

test('one hundred', (t) => {
  t.is('one hundred', toWords(100))
})

test('one hundred and one', (t) => {
  t.is('one hundred and one', toWords(101))
})

test('one hundred and eleven', (t) => {
  t.is('one hundred and eleven', toWords(111))
})

test('one hundred twenty one', (t) => {
  t.is('one hundred and twenty-one', toWords(121))
})

test('113', (t) => {
  t.is('one hundred and thirteen', toWords(113))
})

test('negative numbers throw', (t) => {
  t.throws(() => toWords(-1), outOfBoundsException.message)
})

test('zero', t => t.is(toWords(0), 'zero'))
test('one thousand', t => t.is(toWords(1000), 'one thousand'))

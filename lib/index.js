export const outOfBoundsException = {
  message: 'Number out of bounds',
  name: 'OutOfBoundsException'
}

const ZERO = 'zero'
const HUNDRED = 'hundred'
const UNITS_TO_WORDS = {
  1: 'one', 2: 'two', 3: 'three',
  4: 'four', 5: 'five', 6: 'six',
  7: 'seven', 8: 'eight', 9: 'nine'
}
const TENS_TO_WORDS = {
  10: 'ten', 20: 'twenty', 30: 'thirty',
  40: 'forty', 50: 'fifty', 60: 'sixty',
  70: 'seventy', 80: 'eighty', 90: 'ninety'
}
const TEENS_TO_WORDS = {
  10: 'ten',
  11: 'eleven', 12: 'twelve', 13: 'thirteen',
  14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
  17: 'seventeen', 18: 'eighteen', 19: 'nineteen'
}

const divAndMod = (n, d) => [Math.floor(n / d), n % d]

const toTens = (n) => {
  const [ts, us] = divAndMod(n, 10)
  const unit = UNITS_TO_WORDS[us]

  switch (true) {
  case ts === 0:
    return unit || ZERO
  case ts === 1:
    return TEENS_TO_WORDS[n]
  default:
    return `${TENS_TO_WORDS[ts * 10]}${unit ? `-${unit}` : ''}`
  }
}

const toHundreds = (n) => {
  const [hs, ts] = divAndMod(n, 100)

  return ''
    .concat(hs > 0 ? `${toTens(hs)} ${HUNDRED}` : '')
    .concat(ts > 0 ? `${hs > 0 ? ' and ' : ''}${toTens(ts)}` : '')
    .concat(hs === 0 && ts === 0 ? toTens(ts) : '')
}

export const toWords = (n) => {
  if (n < 0 || n > 1000) throw Object.create(outOfBoundsException)
  if (n === 1000) return 'one thousand'

  return toHundreds(n)
}
